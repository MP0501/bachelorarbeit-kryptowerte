
const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const fs = require('fs');

puppeteer.use(StealthPlugin());

async function scrapeWebsite() {
    // Launch the browser
    const browser = await puppeteer.launch({
	headless:true,
   args: ['--no-sandbox']

});

    // Create a new page
    const page = await browser.newPage();

    let commentsArr = [];

    for(i = 1; i < 650; i++){
	try{
        // Navigate to the provided URL
        await page.goto("https://www.investing.com/crypto/ethereum/chat/"+i);

        // Example: Extracting the title of the webpage
        await page.waitForSelector('#comments_new', {timeout: 300000});

        // Extracting comments and dates
        const commentsData = await page.evaluate(() => {
            const commentsContainer = document.querySelector('#comments_new');
            const comments = Array.from(commentsContainer.querySelectorAll('.border-t'));
            
            const commentsData = comments.map((comment) => {
                const dateElement = Array.from(comment.querySelectorAll('span'))[0];
                const commentContentElement = comment.querySelector('.leading-5.break-words');
                const dateString = dateElement ? dateElement.textContent.trim() : '';

                const parsedDate = new Date();
                if(dateElement){
                    const now = new Date();
                    
                    if (dateString.includes('ago')) {
                        const [, value, unit] = dateString.match(/(\d+) (\w+) ago/);
                        if (unit === 'hour') {
                            parsedDate.setHours(now.getHours() - parseInt(value));
                        } else if (unit === 'minute') {
                            parsedDate.setMinutes(now.getMinutes() - parseInt(value));
                        }
                    } else {
                        parsedDate.setTime(Date.parse(dateString));
                    }
                }
                            
                return {
                    comment: commentContentElement.textContent.trim(),
                    date: parsedDate.toLocaleString() // Handle if date element is not found
                };
            });

            return commentsData;
        });

        commentsArr = commentsArr.concat(commentsData)
	console.log("Success! "+ commentsData.length +" comments found on page "+i)

        //await page.waitForTimeout((Math.floor(Math.random() * 12) + 1) * 1000) 
	}catch(err){
		console.log("Error! on page "+i)

		i = i-1
	}
    }

    const csvData = commentsArr.map(comment => `"${comment.comment}","${comment.date}"`).join('\n');
    fs.writeFileSync('comments_ethereum.csv', `Comment,Date\n${csvData}`);

    // Log the extracted comments and dates

    // Close the browser
    await browser.close();
}

scrapeWebsite()