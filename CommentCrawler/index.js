
const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const fs = require('fs');

//Hier wird das Stealth Plugin genutzt, um die Bot Protection zu umgehen
puppeteer.use(StealthPlugin());

async function scrapeWebsite() {
    //Starten des Headless Chorme Browsers
    const browser = await puppeteer.launch({
	headless:true,
   args: ['--no-sandbox']

});
    
    const page = await browser.newPage();

    let commentsArr = [];
    
    //Auf 650 Seiten sind alle Beiträge für Etherum bis 31.12.2021 abgebildet
    for(i = 1; i < 650; i++){
        try{
            await page.goto("https://www.investing.com/crypto/ethereum/chat/"+i);
            
            //Warten bis die Seite geladen hat
            await page.waitForSelector('#comments_new', {timeout: 300000});

            const commentsData = await page.evaluate(() => {
                //Beiträge können mit diesen Selektoren gefiltert werden
                const commentsContainer = document.querySelector('#comments_new');
                const comments = Array.from(commentsContainer.querySelectorAll('.border-t'));

                const commentsData = comments.map((comment) => {
                    const dateElement = Array.from(comment.querySelectorAll('span'))[0];
                    const commentContentElement = comment.querySelector('.leading-5.break-words');
                    const dateString = dateElement ? dateElement.textContent.trim() : '';
                    
                    //Veröffentlichungsdatum liegt leider nicht im richtigen Vormat vor
                    const parsedDate = new Date();
                    if(dateElement){
                        const now = new Date();

                        if (dateString.includes('ago')) {
                            const [, value, unit] = dateString.match(/(\d+) (\w+) ago/);
                            if (unit === 'hour') {
                                parsedDate.setHours(now.getHours() - parseInt(value));
                            } else if (unit === 'minute') {
                                parsedDate.setMinutes(now.getMinutes() - parseInt(value));
                            }
                        } else {
                            parsedDate.setTime(Date.parse(dateString));
                        }
                    }

                    return {
                        comment: commentContentElement.textContent.trim(),
                        date: parsedDate.toLocaleString() 
                    };
                });

                return commentsData;
            });

            commentsArr = commentsArr.concat(commentsData)
            console.log("Success! "+ commentsData.length +" comments found on page "+i)

        }catch(err){
            console.log("Error! on page "+i)

            i = i-1
        }
    }
    
    //Speichern der Artikel in einer CSV Datei
    const csvData = commentsArr.map(comment => `"${comment.comment}","${comment.date}"`).join('\n');
    fs.writeFileSync('comments_ethereum.csv', `Comment,Date\n${csvData}`);

    
    //Auf 7650 Seiten sind alle Beiträge für Bitcoin bis 31.12.2021 abgebildet
    for(i = 1; i < 7650; i++){
        try{
            await page.goto("https://www.investing.com/crypto/bitcoin/chat/"+i);

            await page.waitForSelector('#comments_new', {timeout: 300000});

            const commentsData = await page.evaluate(() => {
                const commentsContainer = document.querySelector('#comments_new');
                const comments = Array.from(commentsContainer.querySelectorAll('.border-t'));

                const commentsData = comments.map((comment) => {
                    const dateElement = Array.from(comment.querySelectorAll('span'))[0];
                    const commentContentElement = comment.querySelector('.leading-5.break-words');
                    const dateString = dateElement ? dateElement.textContent.trim() : '';

                    const parsedDate = new Date();
                    if(dateElement){
                        const now = new Date();

                        if (dateString.includes('ago')) {
                            const [, value, unit] = dateString.match(/(\d+) (\w+) ago/);
                            if (unit === 'hour') {
                                parsedDate.setHours(now.getHours() - parseInt(value));
                            } else if (unit === 'minute') {
                                parsedDate.setMinutes(now.getMinutes() - parseInt(value));
                            }
                        } else {
                            parsedDate.setTime(Date.parse(dateString));
                        }
                    }

                    return {
                        comment: commentContentElement.textContent.trim(),
                        date: parsedDate.toLocaleString() 
                    };
                });

                return commentsData;
            });

            commentsArr = commentsArr.concat(commentsData)
            console.log("Success! "+ commentsData.length +" comments found on page "+i)

        }catch(err){
            console.log("Error! on page "+i)

            i = i-1
        }
    }

    const csvData = commentsArr.map(comment => `"${comment.comment}","${comment.date}"`).join('\n');
    fs.writeFileSync('comments_bitcoin.csv', `Comment,Date\n${csvData}`);
    
    //Auf 350 Seiten sind alle Beiträge für Ripple bis 31.12.2021 abgebildet
    for(i = 1; i < 350; i++){
        try{
            await page.goto("https://www.investing.com/crypto/ripple/chat/"+i);

            await page.waitForSelector('#comments_new', {timeout: 300000});

            const commentsData = await page.evaluate(() => {
                const commentsContainer = document.querySelector('#comments_new');
                const comments = Array.from(commentsContainer.querySelectorAll('.border-t'));

                const commentsData = comments.map((comment) => {
                    const dateElement = Array.from(comment.querySelectorAll('span'))[0];
                    const commentContentElement = comment.querySelector('.leading-5.break-words');
                    const dateString = dateElement ? dateElement.textContent.trim() : '';

                    const parsedDate = new Date();
                    if(dateElement){
                        const now = new Date();

                        if (dateString.includes('ago')) {
                            const [, value, unit] = dateString.match(/(\d+) (\w+) ago/);
                            if (unit === 'hour') {
                                parsedDate.setHours(now.getHours() - parseInt(value));
                            } else if (unit === 'minute') {
                                parsedDate.setMinutes(now.getMinutes() - parseInt(value));
                            }
                        } else {
                            parsedDate.setTime(Date.parse(dateString));
                        }
                    }

                    return {
                        comment: commentContentElement.textContent.trim(),
                        date: parsedDate.toLocaleString() 
                    };
                });

                return commentsData;
            });

            commentsArr = commentsArr.concat(commentsData)
            console.log("Success! "+ commentsData.length +" comments found on page "+i)

        }catch(err){
            console.log("Error! on page "+i)

            i = i-1
        }
    }

    const csvData = commentsArr.map(comment => `"${comment.comment}","${comment.date}"`).join('\n');
    fs.writeFileSync('comments_ripple.csv', `Comment,Date\n${csvData}`);
    
    await browser.close();
}

scrapeWebsite()